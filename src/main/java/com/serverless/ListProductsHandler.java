package com.serverless;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.serverless.dal.Product;
import org.apache.log4j.Logger;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class ListProductsHandler implements RequestHandler<Map<String, Object>, ApiGatewayResponse> {

    private final Logger logger = Logger.getLogger(this.getClass());

    @Override
    public ApiGatewayResponse handleRequest(Map<String, Object> input, Context context) {
        List<Product> products;
        ApiGatewayResponse apiGatewayResponse = null;

        try {
            // get all products
            products = new Product().list();

            apiGatewayResponse= ApiGatewayResponse.builder()
                    .setStatusCode(200)
                    .setObjectBody(products)
                    .setHeaders(Collections.singletonMap("X-Powered-By", "AWS Lambda & Serverless"))
                    .build();

        } catch (Exception ex) {
            logger.error("Error in listing products: " + ex);

            // send the error response back
        }
        // send the response back
        //ApiGatewayResponse apiGatewayResponse1 = apiGatewayResponse;
        return apiGatewayResponse;
    }
}